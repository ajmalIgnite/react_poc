﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
namespace WebApplication1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PostcodeController : ApiController
    {
        [HttpGet, Route("api/postcode/{postcode}/{number}")]
        public DTL.PostcodeLookUp LookUp(String postcode, int number)
        {
            DTL.PostcodeLookUp result = new DTL.PostcodeLookUp();
            using (System.Net.WebClient objClient = new WebClient())
            {
                DTL.PostCodeResponse.GetAddressInfo address = Newtonsoft.Json.JsonConvert.DeserializeObject<DTL.PostCodeResponse.GetAddressInfo>(objClient.DownloadString("https://api.getaddress.io/find/" + postcode + "/" + number + "?api-key=R441nHnwAUeGu44t4bt8og15970"));
                if (address.addresses == null || !address.addresses.Any())
                {
                    return null;
                }
                else
                {
                    String[] addressResponse = address.addresses.First().Split(',');
                    result.Address1 = addressResponse[0].Replace(number.ToString(), String.Empty).Trim();
                    result.Address2 = addressResponse[1];
                    result.Address3 = addressResponse[2];
                    result.Address4 = addressResponse[3];
                    result.Town = addressResponse[5];
                    result.Number = number.ToString();
                    result.Postcode = postcode.ToUpper();
                    result.City = addressResponse[6];
                }
            }
            return result;
        }
    }
}
