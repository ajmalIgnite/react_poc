﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class ConfigurationController : ApiController
    {
        [HttpGet, Route("api/configuration/logo/{Tenant}")]
        public HttpResponseMessage Logo(String Tenant)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            using (var stream = new System.IO.MemoryStream())
            {
                System.Drawing.Image image = System.Drawing.Image.FromFile(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\"+ Tenant + ".jpg");
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
               
             
                result.Content = new ByteArrayContent(stream.ToArray());
                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            }
            return result;
        }
    }
}
