﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using Newtonsoft.Json;
namespace WebApplication1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CarLookupController : ApiController
    {
        [HttpGet,Route("api/CarLookup/{reg}")]
        public DTL.VehicleLookup LookUp(String reg)
        {
            DTL.VehicleLookup lookup = new DTL.VehicleLookup();
            try
            {
                string stream;
                string ApiKey = "4136493C-1BEF-46E6-9DAE-440F5CC2D1F7";
                string url = String.Format("https://uk1.ukvehicledata.co.uk/api/datapackage/{0}?v=2&api_nullitems=1&key_vrm={1}&auth_apikey={2}", "VehicleData", reg, ApiKey);
                WebRequest request = WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (StreamReader streamRdr = new StreamReader(response.GetResponseStream()))
                {
                    stream = streamRdr.ReadToEnd();
                }
                System.Web.HttpContext.Current.Response.AddHeader("Response", stream);
                DTL.VehicleResponse.VehicleDataV2 vehicle = JsonConvert.DeserializeObject<DTL.VehicleResponse.VehicleDataV2>(stream);
                lookup.Make = vehicle.Response.DataItems.VehicleRegistration.Make;
                lookup.Model = vehicle.Response.DataItems.VehicleRegistration.MakeModel;
                lookup.Year = Int32.Parse(vehicle.Response.DataItems.VehicleRegistration.YearOfManufacture);
            }
            catch {
                lookup.Make = "Honda";
                lookup.Model = "FRV";
            }
            return lookup;
        }
    }
}
