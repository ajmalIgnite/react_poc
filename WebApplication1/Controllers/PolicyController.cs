﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
namespace WebApplication1.Controllers
{
    /// <summary>
    /// This controller allows you to create a qoute and retrieve policies
    /// </summary>
    //luke was here 18/10/2018
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PolicyController : ApiController
    {
        /// <summary>
        /// Get all the policies
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/policy/all")]
        public List<DTL.QuoteResponse> All()
        {
            List<DTL.QuoteResponse> result = new List<DTL.QuoteResponse>();
            foreach(var objFile in System.IO.Directory.GetFiles(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Quotes\\"))
            {
                result.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<DTL.QuoteResponse>(System.IO.File.ReadAllText(objFile)));
            }
            return result;
        }
        [HttpPost]
        public DTL.QuoteResponse Create(List<DTL.Question> Questions)
        {
            DTL.QuoteResponse quoteResponse = new DTL.QuoteResponse();

            quoteResponse.Questions = Questions;
            quoteResponse.Errors = BLL.Utility.Validate(Questions);
            if (!quoteResponse.Errors.Any())
            {
                quoteResponse.Value = new Random().Next(1, 200);

                quoteResponse.Date = DateTime.Now;
                quoteResponse.QuoteRef = DateTime.Now.Ticks.ToString();

                System.IO.File.WriteAllText(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Quotes\\" + quoteResponse.QuoteRef + ".json", Newtonsoft.Json.JsonConvert.SerializeObject(quoteResponse));
            }
            return quoteResponse;
        }

    }
}
