﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
namespace WebApplication1.Controllers
{
    public class QuestionController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpGet]
        public List<DTL.Question> All()
        {
            List<DTL.Question> list = new List<DTL.Question>();
            list.Add(new DTL.Question() { Index = 0, Text = "Registration", Children = new List<DTL.Question>() { new DTL.Question() { Text = "Model", Validation = "Text", Required = true, Index = 101 }, new DTL.Question() { Text = "Make", Validation = "Text", Required = true, Index = 101 } }, Validation = "CarLookup", Required = true });
            list.Add(new DTL.Question() { Index = 1, Text = "Vehicle Value", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 2, Text = "Number of seats", Children = new List<DTL.Question>(), Validation = "Number", Required = true });
            list.Add(new DTL.Question() { Index = 3, Text = "Car imported", Children = new List<DTL.Question>(), Validation = "Boolean", Required = true, });
            list.Add(new DTL.Question() { Index = 4, Text = "Mileage", Children = new List<DTL.Question>(), Validation = "Number", Required = true, });
            list.Add(new DTL.Question() { Index = 5, Text = "Is the vehicle modified?", Children = new List<DTL.Question>() { new DTL.Question() { Validation = "Text", Text = "Is it really?" }, new DTL.Question() { Validation = "Boolean", Text = "Are you sure?" } }, Validation = "Boolean", Required = true });
            list.Add(new DTL.Question() { Index = 6, Text = "Is the vehicle fitted with a tracking device?", Children = new List<DTL.Question>(), Validation = "Boolean", Required = true });
            list.Add(new DTL.Question() { Index = 7, Text = "Who is the owner of the vehicle?", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 8, Text = "Who is the keeper of the vehicle?", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 9, Text = "Overnight park location?", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 10, Text = "What is your driving licence number?", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 11, Text = "Title", Children = new List<DTL.Question>(), Validation = "Option", Required = true, Options = new List<string>() { "Mr", "Mrs" } });
            list.Add(new DTL.Question() { Index = 12, Text = "Forename", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 13, Text = "Surname", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 14, Text = "Email address", Children = new List<DTL.Question>(), Validation = "Email" });
            list.Add(new DTL.Question() { Index = 15, Text = "Date of Birth", Children = new List<DTL.Question>(), Validation = "Date" });
            list.Add(new DTL.Question() { Index = 15, Text = "House Number/Flat Number", Children = new List<DTL.Question>(), Validation = "Number", ConditionValue = "POSTCODE" });
            list.Add(new DTL.Question() { Index = 16, Text = "Post code", Children = new List<DTL.Question>() { new DTL.Question() { Text = "Address 1", Validation = "Text" }, new DTL.Question() { Text = "Address 2", Validation = "Text" }, new DTL.Question() { Text = "Address 3", Validation = "Text" }, new DTL.Question() { Text = "Address 4", Validation = "Text" }, new DTL.Question() { Text = "Town", Validation = "Text" } , new DTL.Question() { Text = "City", Validation = "Text" } }, Validation = "Postcode" });
            list.Add(new DTL.Question() { Index = 17, Text = "Employment Status", Children = new List<DTL.Question>(), Validation = "Option", Required = true, Options = new List<string>() { "Employed", "Self-employed" } });
            list.Add(new DTL.Question() { Index = 18, Text = "Drivers to be insured?", Children = new List<DTL.Question>(), Validation = "Text", Required = true });
            list.Add(new DTL.Question() { Index = 19, Text = "Where did you hear about us?", Children = new List<DTL.Question>(), Validation = "Label", Required = false });
            list.Add(new DTL.Question() { Index = 20, Text = "Confirm the following assumptions are correct: \nYou are a UK Resident since birth \nYou have no Medical Conditions \nYou have had no unspent non - motoring criminal convictions", Children = new List<DTL.Question>(), Validation = "Boolean", Required = true });
            list.Add(new DTL.Question() { Index = 21, Text = "Yoga would also like to keep you informed about products, services, offers and promotions Yoga make available. Can Yoga contact you by:", Children = new List<DTL.Question>() { new DTL.Question() { Index = 22, Text = "Email", Children = new List<DTL.Question>(), Validation = "Boolean", Required = true }, new DTL.Question() { Index = 23, Text = "SMS", Children = new List<DTL.Question>(), Validation = "Boolean", Required = true } }, Validation = "Label" });
            return list;
        }
    }
}
