﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTL
{
    public class Question
    {
        public int Index{ get; set; }
        public String Text { get; set; }
        public String Value { get; set; }
        public enum ConditionType { YesNo, Text }
        public String ConditionValue { get; set; }
        public String Validation { get; set; }
        public List<Question> Children { get; set; }
        public bool Required { get; set; }
        public List<String> Options { get; set; }
        public String Error { get; set; }
    }
}
