﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTL.PostCodeResponse
{
    public class GetAddressInfo
    {
        public String[] addresses { get; set; }
        public String latitude { get; set; }
        public String longitude { get; set; }
    }
}
