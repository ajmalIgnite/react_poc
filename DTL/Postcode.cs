﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTL
{
    public class PostcodeLookUp
    {
        public String Number { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String Address4 { get; set; }
        public String Town { get; set; }
        public String City { get; set; }
        public String Postcode { get; set; }
    }
}
