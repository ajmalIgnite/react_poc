﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTL
{
    public class QuoteResponse
    {
        public DateTime? Date { get; set; }
        public bool Accepted { get; set; }
        public decimal Value { get; set; }
        public string QuoteRef { get; set; }
        public List<Question> Questions { get; set; }
        public List<Error> Errors { get; set; }
        public class Error
        {
          
            public String ErrorMessage { get; set; }
            public Question Question { get; set; }
        }
    }
}
