﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTL
{
    public class VehicleLookup
    {
        public String Registration { get; set; }
        public String Model { get; set; }
        public String Make { get; set; }
        public Int32 Year { get; set; }
    }
}
