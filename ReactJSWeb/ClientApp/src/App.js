import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { PolicyCreate } from './components/PolicyCreate';
import { Quotes } from './components/Quotes';
import { Counter } from './components/Counter';

export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
            <Route path='/PolicyCreate' component={PolicyCreate} />
            <Route path='/Quotes' component={Quotes} />
      </Layout>
    );
  }
}
