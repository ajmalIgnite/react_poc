import React, { Component } from 'react';
import $ from 'jquery'
import ko from 'knockout'
import jqv from 'jquery-validation'
export class PolicyCreate extends Component {
    displayName = PolicyCreate.name;

    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true, };

        fetch('http://localhost:50622/api/question/all')
            .then(response => response.json())
            .then(data => {

                var vmQuestions = new function () {
                    var self = this;
                    this.message = ko.observable('');
                    this.questions = ko.observableArray(data);
                    this.form = $("#divQuestions");
                    this.form.validate();
                    this.getQuote = function () {
                        if (self.form.valid()) {
                            fetch('http://localhost:50622/api/policy/create', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(self.questions())
                            }).then(response => response.json()).then(data => {

                                self.message('You have a quote for ' + data.Value);

                            });
                        } else {
                            self.form.validate().focusInvalid();
                        }
                    };
                }
                ko.applyBindings(vmQuestions, $("#divQuestions")[0]);
            });


    }



    static renderQuestionTable(questions) {
        return (
            <form id="divQuestions">
                <table className='table' data-bind="visible: questions">
                    <thead>
                        <tr>
                            <th className="table-cell">Question</th>
                            <th>Value</th>

                        </tr>
                    </thead>
                    <tbody data-bind="foreach: questions">

                        <tr>
                            <td data-bind="text: Text "></td>
                            <td>
                                <div data-bind="if: Validation == 'Text'">
                                    <input data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                                <div data-bind="if: Validation == 'Postcode'">
                                    <input data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                                <div data-bind="if: Validation == 'Boolean'">
                                    <input type="checkbox" data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                                <div data-bind="if: Validation == 'Option'">
                                    <select data-bind="checked: Value, attr: {'required': Required? 'true' : 'false'}, foreach: Options">
                                        <option data-bind="text: $data"></option>
                                    </select>
                                </div>
                                <div data-bind="if: Validation == 'Email'">
                                    <input type="Email" data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                                <div data-bind="if: Validation == 'Number'">
                                    <input type="Number" data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                                <div data-bind="if: Validation == 'Date'">
                                    <input type="datetime-local" data-bind="value: Value, attr: {'required': Required? 'true' : 'false'}" />
                                </div>
                            </td>
                        </tr>


                    </tbody>
                </table>
            
                <button data-bind="click: getQuote.bind() ">Get Quote</button>

                <h2 data-bind="text: message"></h2>
            </form>
           

        );
    }


    render() {
        let contents = PolicyCreate.renderQuestionTable(this.state.questions);

        return (
            <div>
                <h1>Create a quote</h1>
                <p>Please enter the details below to create a quote.</p>
                {contents}


            </div>
        );
    }
}
