import React, { Component } from 'react';
import $ from 'jquery';
import jqv from 'jquery-validation';
import ko from 'knockout';
import services from '../Services';

export class PolicyCreate extends Component {
    static childQuestion() {
        return (
            <div>
                <div data-bind="text: Text + (Required? '*' : '')" />

                <div>
                    <div data-bind="if: Validation == 'Text'">
                        <input data-bind="value: Value, attr:{'name' : Index },  attr:{'id' : Index },  attr: Required == false? {}: {'required': 'true'}" />
                    </div>
                    <div data-bind="if: Validation == 'Postcode'">
                        <input data-bind="value: Value, attr: Required == false? {}: {'required':'true'}" />
                    </div>
                    <div data-bind="if: Validation == 'Boolean'">
                        <input type="checkbox" data-bind="click: $root.checkBoxClick.bind(),  checked: Value, attr: Required == false? {}:{'required': 'true'}" />
                    </div>
                    <div data-bind="if: Validation == 'Option'">
                        <select data-bind="checked: Value, attr: Required == false? {}: {'required': 'true'}, foreach: Options">
                            <option data-bind="text: $data" />
                        </select>
                    </div>
                    <div data-bind="if: Validation == 'Email'">
                        <input type="email" data-bind="value: Value, attr: Required == false? {}: {'required': 'true'}" pattern="^[a-z0-9].*" />
                    </div>
                    <div data-bind="if: Validation == 'Number'">
                        <input type="Number" data-bind="value: Value, attr:  Required == false? {}:{'required': 'true'}" />
                    </div>
                    <div data-bind="if: Validation == 'Date'" min="2000-01-02">
                        <input type="datetime-local" data-bind="value: Value, attr: Required == false? {}: {'required': 'true'}" />
                    </div>
                </div>
            </div>
        );
    }
    static renderQuestionTable(questions) {
        return (
            <form ref="template">
                <h2 data-bind="html: message" />
                <div data-bind="visible: errors, foreach: errors">
                    <div data-bind="style: {'color': 'red'} ,text: ErrorMessage" />
                </div>
                <button data-bind="visible: hasQoute, click: $root.proceed.bind()">Proceed with quote...</button>
                <table className='table' data-bind="visible: questions != null">
                    <thead>
                        <tr>
                            <th className="table-cell">Question</th>
                            <th>Answer</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: questions">
                        <tr>
                            <td data-bind="text: Text + (Required? '*' : '')" />
                            <td>
                                <div data-bind="if: Validation == 'Text'">
                                    <input data-bind="value: Value, attr:{'name' : Index },  attr:{'id' : Index },  attr: Required == false? {}: {'required': 'true'}" />
                                </div>
                                <div data-bind="if: Validation == 'CarLookup'">
                                    <input id="dvla" data-bind="value: Value, attr:{'name' : Index },  attr:{'id' : Index },  attr: Required == false? {}: {'required': 'true'}" />
                                    <button data-bind="click: $root.dvlaLookUp.bind($root)">Find...</button>
                                    <div data-bind="style: {'display': 'none'} , attr: {'id' : Index + 'child'}">

                                        <div data-bind="with: $root.car">
                                            <div data-bind="text: 'Make'" />
                                            <input data-bind="value: Make" />
                                            <div data-bind="text: 'Model'" />
                                            <input data-bind="value: Model" />

                                        </div>

                                    </div>
                                </div>
                                <div data-bind="if: Validation == 'Postcode'">
                                    <input id="postcode" data-bind="value: Value, attr:{'name' : Index },  attr:{'id' : Index },  attr: Required == false? {}: {'required': 'true'}" />
                                    <button data-bind="click: $root.addressLookUp.bind($root)">Find...</button>
                                    <div data-bind="style: {'display': 'none'} , attr: {'id' : Index + 'child'}">
                                   
                                        <div data-bind="with: $root.address">
                                                <div data-bind="text: 'Address 1'" />
                                                <input data-bind="value: Address1" />
                                                <div data-bind="text: 'Address 2'" />
                                                <input data-bind="value: Address2" />
                                            <div data-bind="text: 'Address 3'" />
                                            <input data-bind="value: Address3" />
                                            <div data-bind="text: 'Address 4'" />
                                            <input data-bind="value: Address4" />
                                            <div data-bind="text: 'Town'" />
                                            <input data-bind="value: Town" />
                                            <div data-bind="text: 'City'" />
                                            <input data-bind="value: City" />
                                            </div>
                                     
                                    </div>
                                </div>
                                <div data-bind="if: Validation == 'Boolean'">
                                    <input type="checkbox" data-bind="click: $root.checkBoxClick.bind(),  checked: Value, attr: Required == false? {}:{'required': 'true'}" />
                                    <div data-bind="style: {'display': 'none'} , attr: {'id' : Index + 'child'}">
                                        <table>

                                            <div data-bind="foreach: Children">
                                                {PolicyCreate.childQuestion()}
                                            </div>

                                        </table>
                                    </div>
                                </div>
                                <div data-bind="if: Validation == 'Label'">
                                    <div>
                                        <table>

                                            <div data-bind="foreach: Children">
                                                {PolicyCreate.childQuestion()}
                                            </div>
                                  
                                        </table>
                                    </div>
                                </div>
                                
                                <div data-bind="if: Validation == 'Option'">
                                    <select data-bind="value: Value, attr: Required == false? {}: {'required': 'true'}, foreach: Options">
                                        <option data-bind="text: $data" />
                                    </select>
                                </div>
                                <div data-bind="if: Validation == 'Email'">
                                    <input type="email" data-bind="value: Value, attr: Required == false? {}: {'required': 'true'}" pattern="^[a-z0-9].*" />
                                </div>
                                <div data-bind="if: Validation == 'Number'">
                                    <input type="Number" data-bind="value: Value, attr:  Required == false? {}:{'required': 'true'}" />
                                </div>
                                <div data-bind="if: Validation == 'Date'" min="2000-01-02">
                                    <input type="date" step="7200" data-bind="value: Value, attr: Required == false? {}: {'required': 'true'}" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button type="submit" data-bind="click: $root.getQuote.bind() ">Get Quote</button>
            </form>
        );
    }
    constructor(props) {
        super(props);
        this.hasQoute = ko.observable(false);
        this.hasCarLookup = ko.observable(false);
        this.message = ko.observable('');
        this.questions = ko.observableArray(null);
        this.car = ko.observable();
        this.address = ko.observable();
        this.errors = ko.observableArray(null);
        fetch(services.baseUrl + 'api/question/all')
            .then(response => response.json())
            .then(data => {
                this.form = $(this.refs.template);

                this.hasQoute = ko.observable(false);
                this.message = ko.observable('');

                this.questions = ko.observableArray(data);
                ko.applyBindings(this, this.form[0]);
            });
    }
    addressLookUp = function (sender, root) {
        $("#" + sender.Index + "child").show();
        var housenumber = 0;
        this.questions().forEach(function (question) {
            if (question.Text == 'House Number/Flat Number') {
                housenumber = parseInt(question.Value);
            }
        });
        fetch(services.baseUrl + 'api/postcode/' + ($("#postcode").val() === '' ? 'NA' : $("#postcode").val()) + '/' + housenumber)
            .then(response => response.json())
            .then(data => {
                this.address(data);
                sender.Children.forEach(function (question) {
                    if (question.Text === "Make") {
                        question.Value = data.Make;
                    }
                    if (question.Text === "Model") {
                        question.Value = data.Model;
                    }
                });
           //luke was here again 31/10/2018
            });
    }
    dvlaLookUp = function (sender, root) {
        $("#" + sender.Index + "child").show();
        fetch(services.baseUrl + 'api/CarLookup/' + ($("#dvla").val() === '' ? 'NA' : $("#dvla").val()))
            .then(response => response.json())
            .then(data => {
             
                sender.Children.forEach(function (question) {
                    if (question.Text === "Make") {
                        question.Value = data.Make;
                    }
                    if (question.Text === "Model") {
                        question.Value = data.Model;
                    }
                });
                this.car(data);
            });
    }
    checkBoxClick = function (sender) {

        if (sender.Value !== true) {
            $("#" + sender.Index + "child").show();
        } else {
            $("#" + sender.Index + "child").hide();
        }
        return true;
    }
    populate = function (data, form) {
        ko.applyBindings(this, this.form);
        this.form = form;
        this.form.validate();
        this.hasQoute = ko.observable(false);
        this.message = ko.observable('');
        this.questions(data);

    }

    getQuote = function (sender) {
        fetch(services.baseUrl + 'api/policy/create', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(sender.questions())
        }).then(response => response.json()).then(data => {
            sender.errors(data.Errors);
            if (data.Errors.length == 0) {
                sender.message('You have a quote for &pound;' + data.Value);
                sender.hasQoute(true);
            }
        });

        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
    proceed = function (sender) {
        alert('buy qoute');
    }


    render() {
        return (
            <div>
                <h1>Create a quote</h1>
                <p>Please enter the details below to create a quote.</p>
                {PolicyCreate.renderQuestionTable()}
                <br />

            </div>

        );
    }
}
