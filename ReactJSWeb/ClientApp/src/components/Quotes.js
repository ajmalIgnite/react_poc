import React, { Component } from 'react';
import $ from 'jquery'
import ko from 'knockout'
import moment from 'moment'
import jqv from 'jquery-validation'
export class Quotes extends Component {
   static renderQuotes() {
        return (
            <form id="quotes">
                <table className='table' data-bind="visible: quotes">
                    <thead>
                        <tr>
                            <th className="table-cell">ID</th>
                            <th>Date</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: quotes">
                        <tr>
                            <td data-bind=""></td>
                            <td data-bind="text: QuoteRef "></td>
                            <td data-bind="text: $root.formatDate(Date) "></td>
                            <td data-bind="text: '&pound;' + Value "></td>
                            <td><button data-bind="click: $root.select.bind(this)">View</button></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }
    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true };

        fetch('http://localhost:50622/api/policy/all')
            .then(response => response.json())
            .then(data => {
                ko.applyBindings(new Model(data), $("#quotes")[0]);
            });
    }
    render() {
        return (
            <div>
                <h1>Your Quotes</h1>
                <p>Here is a list of your quotes.</p>
                {Quotes.renderQuotes()}


            </div>
        );
    }
}
export class Model {
    quotes = ko.observableArray([]);
    constructor(data) {
        this.quotes(data);
    }
    formatDate = function (date) {
    
        return moment(date).format('LL');
    };

    select = function (quote) {
        alert(quote);
        alert(quote.QuoteRef);
    };

}