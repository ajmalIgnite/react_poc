﻿import $ from 'jquery'
import ko from 'knockout'
export class PolicyCreateViewModel {
    constructor(data, form) {
        this.hasQoute = ko.observable(false);
        this.message = ko.observable('');
        this.questions = ko.observableArray([]);

    }
    Populate = function (data, form) {
        this.form = form;
        this.hasQoute = ko.observable(false);
        this.message = ko.observable('');
        this.questions = ko.observableArray(data);
        ko.applyBindings(this, this.form);
    }

    GetQuote = function () {
        fetch('http://localhost:50622/api/policy/create', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.questions())
        }).then(response => response.json()).then(data => {

            this.message('You have a quote for &pound;' + data.Value);
            this.hasQoute(true);
        });

        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
    Proceed = function (quote) {
        if (!this.form.valid()) {
            //self.form.focusInvalid();
            return;
        }
        fetch('http://localhost:50622/api/policy/create', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.questions())
        }).then(response => response.json()).then(data => {

            this.message('You have a quote for &pound;' + data.Value);
            this.hasQoute(true);
        });

        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
}