﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace BLL
{
    public class Utility
    {
        static Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        public static List<DTL.QuoteResponse.Error> Validate(List<DTL.Question> Questions)
        {
            List<DTL.QuoteResponse.Error> result = new List<DTL.QuoteResponse.Error>();
            foreach (DTL.Question question in Questions)
            {
                bool check = true;
                if (question.Required)
                {
                    if (String.IsNullOrEmpty(question.Value) && question.Value != "Boolean")
                    {
                        result.Add(new DTL.QuoteResponse.Error() { ErrorMessage = "You did not answer the required question '"+question.Text+"'", Question = question});
                        check = false;
                    }
                }
                if (check)
                {
                    switch ((question.Validation ?? String.Empty).ToUpper())
                    {
                        case "EMAIL":
                            try
                            {
                                System.Net.Mail.MailAddress address = new System.Net.Mail.MailAddress(question.Value);
                            }
                            catch
                            {
                                result.Add(new DTL.QuoteResponse.Error() { Question = question, ErrorMessage = "Invalid email address specified" });
                            }
                          
                            break;
                        case "DATE":
                            DateTime outDt = DateTime.MinValue;
                            if (!DateTime.TryParse(question.Value ?? String.Empty, out outDt))
                            {
                                result.Add(new DTL.QuoteResponse.Error() { ErrorMessage = "You need to provide a valid date for '" + question.Text + "'", Question = question });
                            }
                            else if(outDt < new DateTime(1900,1,1))
                            {
                                result.Add(new DTL.QuoteResponse.Error() { ErrorMessage = "You need to provide a valid date for '" + question.Text + "'", Question = question });

                            }
                            break;
                        case "NUMBER":
                            int outInt = 0;
                            if (!Int32.TryParse(question.Value ?? String.Empty, out outInt))
                            {
                                result.Add(new DTL.QuoteResponse.Error() { ErrorMessage = "You need to provide a valid number for '" + question.Text + "'", Question = question });
                            }
                            break;
                    }
                }
            }
       
            return result;
        }
    }
}
